## Run docker image

"As another open source lover, I want to be able to run this software in Docker, so I can check if the service works out of the
box."

`docker-compose up --build -d`

## POST a simple operation

`curl -d '{"operate": "3 + 2"}' -H 'Content-Type: application/json' http://localhost:6543/api/v1/operate`

## Run unit tests

`docker-compose exec web python -m unittest tests.py`

## Read docs

[Run](#run-docker-image) then go to `http://localhost:6543/api/v1/`
import unittest
from pyramid import testing

from app import simple_operation
from handler import CalculationHandler

"""
As a developer, I want measures in the repository that make sure we do not break the API when we refactor the code in 4
years, so I create code of a high quality.
"""


class CalculationIntegrationTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def test_view_fn_requires_json(self):
        request = testing.DummyRequest()
        request.context = testing.DummyResource()
        response = simple_operation(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json_body.get("message"),
                         "request must contain a JSON object with the operate attribute")

    def test_view_fn_empty_request(self):
        request = testing.DummyRequest(json_body={}, method='POST')
        request.context = testing.DummyResource()
        response = simple_operation(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json_body.get("message"),
                         "request must contain a JSON object with the operate attribute")

    def test_view_fn_simple_operation_addition(self):
        request = testing.DummyRequest(json_body={"operate": "3 + 2"}, method='POST')
        request.context = testing.DummyResource()
        response = simple_operation(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json_body.get("result"), 5.0)

    def test_view_fn_simple_operation_multiplication(self):
        request = testing.DummyRequest(json_body={"operate": "3 * 2"}, method='POST')
        request.context = testing.DummyResource()
        response = simple_operation(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json_body.get("result"), 6.0)

    def test_view_fn_simple_operation_division(self):
        request = testing.DummyRequest(json_body={"operate": "3 / 2"}, method='POST')
        request.context = testing.DummyResource()
        response = simple_operation(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json_body.get("result"), 1.5)

    def test_view_fn_simple_operation_subtraction(self):
        request = testing.DummyRequest(json_body={"operate": "3 - 2"}, method='POST')
        request.context = testing.DummyResource()
        response = simple_operation(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json_body.get("result"), 1.0)

    def test_view_fn_simple_operation_bad_request(self):
        request = testing.DummyRequest(json_body={"operate": "invalid operation"}, method='POST')
        request.context = testing.DummyResource()
        response = simple_operation(request)
        self.assertEqual(response.status_code, 400)


    def tearDown(self):
        testing.tearDown()


class CalculationHandlerTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp()

    def test_handler_ok(self):
        operation_request = "3 + 2"
        self.assertEqual(CalculationHandler(operation_request).calculate(), 5.0)

    def tearDown(self):
        testing.tearDown()

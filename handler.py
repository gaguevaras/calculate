from calculator.simple import SimpleCalculator


class CalculationHandler(object):
    """
    Handle all calculation requests with interchangeable method.

    A a premium developer, I want to implement Object-oriented Programming when possible, so I can maintain this software
later on.

    Currently relies on https://pypi.org/project/simplecalculator/
    """
    def __init__(self, calculation_request):
        self.calculation_request = calculation_request

    def calculate(self):
        """
        Return the result of the simple calculation request

        Encapsulate what changes.

        #@todo: replace this dependency on SimpleCalculator and write our own calculator

        :return: calculation result
        :raises CalculationException:
        """
        try:
            c = SimpleCalculator()
            c.run(self.calculation_request)
            if not c.reg_1_set:
                raise CalculationException("bad input")
            return c.reg_1
        except Exception as e:
            raise CalculationException(e)


class CalculationException(Exception):
    """
    Umbrella exception definition for all calculation exceptions

    # @todo: specify further exceptions to clarify errors shown to end users
    """
    pass
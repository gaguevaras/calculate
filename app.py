from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config

from handler import CalculationHandler, CalculationException


@view_config(route_name="api/v1/operate", openapi=True, renderer='json')
def simple_operation(request):
    """

    Process an API request that contains an operation request and respond
    with the expected result.

    :param request:
    :return:
    """
    if not hasattr(request, "json_body") or "operate" not in request.json_body:
        return Response(
            status=400,
            json_body={
                "message": "request must contain a JSON object with the operate attribute"
            }
        )

    try:
        result = CalculationHandler(request.json_body.get("operate", "")).calculate()
    except CalculationException as ce:
        # @todo:gustavo add Sentry and proper exception handling, for now fail with 400
        # message = ce.message
        """ As a api user, I want a friendly response in case of an error, so I can use this software in production."""
        return Response(status=400, json_body={"message": "calculation failed for reasons x, y and z"})
    return Response(status=200, json_body={"result": result})


if __name__ == '__main__':
    with Configurator() as config:

        config.add_route('operate', 'api/v1/operate')
        config.add_view(simple_operation, route_name='operate')
        config.include("pyramid_openapi3")
        config.pyramid_openapi3_spec('openapi.yml', route='/api/v1/openapi.yml')
        config.pyramid_openapi3_add_explorer(route='/api/v1/')
        app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 6543, app)
    server.serve_forever()
